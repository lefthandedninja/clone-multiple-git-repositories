#!/bin/bash

# Generate a clean project list from your local Git Source directory you want to clone as fresh from your remote repository
ls | sed 's/\///g' > cleanListOfGitProjects.txt

# OPTION1: When remote repository path is static
#Loop through the clean list and append with your remote repository path for mulitple cloning of repositories
while read p; do echo git clone https://YOUR-REMOTE-GIT-PATH/$p; done < cleanListOfGitProjects.txt >> multipleGitCloneProjects.sh

# OPTION2: When remote repository path is different based on specific string within project names
#Loop through the clean list and append with your remote repository path for mulitple cloning of repositories which contain 'ansible-role' string
#while read p; do echo git clone https://YOUR-REMOTE-GIT-PATH/FOLDER_NAME/$p; done < cleanListOfGitProjects.txt | awk '/ansible-role-*/' >> multipleGitCloneProjects.sh